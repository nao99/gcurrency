package gcurrency.job;

import com.posadskiy.currencyconverter.CurrencyConverter;
import gcurrency.entity.CurrencyRatio;
import gcurrency.service.CurrencyCourseService;
import gcurrency.service.CurrencyRatioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyCourseGrabbingJobTest {
    @Autowired
    private CurrencyCourseGrabbingJob currencyCourseGrabbingJob;

    @MockBean
    private CurrencyRatioService currencyRatioService;

    @MockBean
    private CurrencyCourseService currencyCourseService;

    @MockBean
    private CurrencyConverter currencyConverter;

    @Test
    public void testExecute() {
        List<CurrencyRatio> ratios = this.currencyRatioService.getTrackedCurrencyRatios();

        Mockito
                .verify(this.currencyRatioService, Mockito.times(1))
                .getTrackedCurrencyRatios();

        //TODO [glen:19-01-2020]: Add imitation of call the createCurrencyCourses method and test its
    }
}
