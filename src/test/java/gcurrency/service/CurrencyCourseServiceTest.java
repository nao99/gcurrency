package gcurrency.service;

import gcurrency.entity.CurrencyCourse;
import gcurrency.entity.CurrencyRatio;
import gcurrency.repository.CurrencyCourseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyCourseServiceTest {
    @Autowired
    private CurrencyCourseService currencyCourseService;

    @MockBean
    private CurrencyCourseRepository currencyCourseRepository;

    @Test
    public void testCreateUser() {
        CurrencyCourse course = this.currencyCourseService.createCourse(new CurrencyRatio(), 61.2223);

        Mockito
                .doReturn(course)
                .when(this.currencyCourseRepository)
                .save(ArgumentMatchers.any(CurrencyCourse.class));

        Mockito
                .verify(this.currencyCourseRepository, Mockito.times(1))
                .save(ArgumentMatchers.any(CurrencyCourse.class));
    }
}
