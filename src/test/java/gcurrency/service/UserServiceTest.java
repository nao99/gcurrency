package gcurrency.service;

import gcurrency.entity.User;
import gcurrency.repository.UserGroupRepository;
import gcurrency.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserGroupRepository userGroupRepository;

    @Test
    public void testGetUser() {
        Optional<User> user = this.userService.getUser(1);

        Mockito
                .verify(this.userRepository, Mockito.times(1))
                .findById(ArgumentMatchers.anyInt());
    }
}
