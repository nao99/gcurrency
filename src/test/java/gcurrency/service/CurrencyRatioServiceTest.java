package gcurrency.service;

import gcurrency.entity.CurrencyRatio;
import gcurrency.repository.CurrencyRatioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyRatioServiceTest {
    @Autowired
    private CurrencyRatioService currencyRatioService;

    @MockBean
    private CurrencyRatioRepository currencyRatioRepository;

    @Test
    public void testGetTrackedCurrencyRatios() {
        List<CurrencyRatio> ratios = this.currencyRatioService.getTrackedCurrencyRatios();

        Mockito
                .verify(this.currencyRatioRepository, Mockito.times(1))
                .findAllByIsTracked(ArgumentMatchers.eq(Boolean.TRUE));
    }
}
