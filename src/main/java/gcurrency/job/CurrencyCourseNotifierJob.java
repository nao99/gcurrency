package gcurrency.job;

import gcurrency.service.UserService;
import gcurrency.service.api.TelegramApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * CurrencyCourseNotifierJob class
 *
 * Class that every 60 minutes tries to notify
 * the User about currency course changes (up or down)
 * to the telegram messenger
 */
@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
public class CurrencyCourseNotifierJob {
    private TelegramApiService telegramApiService;
    private UserService userService;
    private Logger logger;

    /**
     * CurrencyCourseNotifierJob constructor
     */
    @Autowired
    public CurrencyCourseNotifierJob(TelegramApiService telegramApiService, UserService userService) {
        this.telegramApiService = telegramApiService;
        this.userService = userService;
        this.logger = LoggerFactory.getLogger(CurrencyCourseNotifierJob.class);
    }

    /**
     * Main method
     */
    @Scheduled(cron = "0 5 */1 * * *")
    public void execute() {

    }
}
