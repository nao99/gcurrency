package gcurrency.job;

import com.posadskiy.currencyconverter.CurrencyConverter;
import gcurrency.config.AppProperties;
import gcurrency.entity.CurrencyRatio;
import gcurrency.service.CurrencyCourseService;
import gcurrency.service.CurrencyRatioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

/**
 * CurrencyCourseJob class
 *
 * Class that every 60 minutes takes a currency course
 * and writes its to the database
 */
@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
public class CurrencyCourseGrabbingJob {
    private CurrencyRatioService currencyRatioService;
    private CurrencyCourseService currencyCourseService;
    private CurrencyConverter currencyConverter;
    private Logger logger;

    /**
     * CurrencyCourseJob constructor
     */
    @Autowired
    public CurrencyCourseGrabbingJob(CurrencyRatioService currencyRatioService,
                                     CurrencyCourseService currencyCourseService,
                                     AppProperties appProperties) {

        this.currencyRatioService = currencyRatioService;
        this.currencyCourseService = currencyCourseService;
        this.currencyConverter = new CurrencyConverter(appProperties.getApiKey());
        this.logger = LoggerFactory.getLogger(CurrencyCourseGrabbingJob.class);
    }

    /**
     * Main method
     * Searches an available currency ratios
     * and updates (creates new records) their
     */
    @Scheduled(cron = "0 * */1 * * *")
    public void execute() {
        this.logger.info("Currency course job has started");

        List<CurrencyRatio> currencyRatios = this.currencyRatioService.getTrackedCurrencyRatios();
        if (currencyRatios.isEmpty()) {
            this.logger.info("Unable to find an available currency ratios");
            return;
        }

        this.createCurrencyCourses(currencyRatios);
    }

    /**
     * Method that gets a new currency courses
     * and creates new CurrencyCourse records
     */
    private void createCurrencyCourses(List<CurrencyRatio> currencyRatios) {
        this.logger.info(String.format("Was found %d currency ratios", currencyRatios.size()));

        currencyRatios.forEach(ratio -> {
            this.logger.info(String.format("Currency ratio %d is grabbing", ratio.getId()));

            try {
                Double coefficient = this.currencyConverter.rate(
                        ratio.getDependentCurrency().getName(),
                        ratio.getIndependentCurrency().getName()
                );

                this.logger.info(String.format("Currency ratio %d: Got the coefficient %f", ratio.getId(), coefficient));

                this.currencyCourseService.createCourse(ratio, coefficient);

                this.logger.info("Currency course has successfully saved");
            } catch (RuntimeException e) {
                this.logger.debug(String.format("An error has occurred: %s", e.getMessage()));
            }
        });
    }
}
