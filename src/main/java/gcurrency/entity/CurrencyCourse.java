package gcurrency.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "currency_course")
public class CurrencyCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "currency_ratio_id", nullable = false)
    private CurrencyRatio currencyRatio;

    @Column(name = "coefficient", nullable = false)
    private Double coefficient;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    /**
     * CurrencyCourse constructor
     */
    public CurrencyCourse(CurrencyRatio currencyRatio, Double coefficient) {
        this.currencyRatio = currencyRatio;
        this.coefficient = coefficient;
    }

    @PrePersist
    @PreUpdate
    private void updateTimestamps() {
        if (this.createdAt == null) {
            this.createdAt = new Date();
        }

        this.updatedAt = new Date();
    }
}
