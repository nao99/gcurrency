package gcurrency.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "currency_ratio")
public class CurrencyRatio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "dependent_currency_id", nullable = false)
    private Currency dependentCurrency;

    @ManyToOne
    @JoinColumn(name = "independent_currency_id", nullable = false)
    private Currency independentCurrency;

    @Column(name = "is_tracked")
    private Boolean isTracked = true;
}
