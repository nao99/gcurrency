package gcurrency.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_group")
public class UserGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 25, nullable = false)
    private String name;
}
