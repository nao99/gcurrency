package gcurrency.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "currency")
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 65, nullable = false)
    private String name;
}
