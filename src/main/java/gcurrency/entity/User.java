package gcurrency.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_group_id", nullable = false)
    private UserGroup userGroup;

    @Column(name = "name", length = 32, nullable = false)
    private String name;

    @Column(name = "login", length = 32, nullable = false)
    private String login;

    @Column(name = "ip_address", length = 15, nullable = false)
    private String ipAddress;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserCurrency> currencies;

    @PrePersist
    @PreUpdate
    private void updateTimestamps() {
        if (this.createdAt == null) {
            this.createdAt = new Date();
        }

        this.updatedAt = new Date();
    }
}
