package gcurrency.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * AppProperties class
 * Class that contains all custom properties
 * that wrote in application.properties file
 */
@Configuration
@ConfigurationProperties(prefix = "gcurrency")
@Data
@Validated
public class AppProperties {
    @NotNull
    private String apiKey;
}
