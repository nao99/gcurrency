ALTER TABLE currency_course
    DROP COLUMN dependent_currency_cost,
    DROP COLUMN independent_currency_cost;

CREATE TABLE currency_ratio (
    id                      SERIAL PRIMARY KEY,
    dependent_currency_id   INTEGER NOT NULL
        CONSTRAINT FK_dependent_currency_ratio__currency
            REFERENCES currency (id)
            ON DELETE CASCADE
            ON UPDATE RESTRICT,
    independent_currency_id INTEGER NOT NULL
        CONSTRAINT FK_independent_currency_ratio__currency
            REFERENCES currency (id)
            ON DELETE CASCADE
            ON UPDATE RESTRICT,
    is_tracked              BOOLEAN NOT NULL DEFAULT true
);

ALTER TABLE currency_course
    DROP CONSTRAINT FK_currency_course_dependent__currency,
    DROP CONSTRAINT FK_currency_course_independent__currency,
    DROP COLUMN dependent_currency_id,
    DROP COLUMN independent_currency_id;

ALTER TABLE currency_course
    ADD COLUMN currency_ratio_id INTEGER NOT NULL,
    ADD CONSTRAINT FR_currency_course__currency_ratio
        FOREIGN KEY (currency_ratio_id) REFERENCES currency_ratio (id)
            ON DELETE RESTRICT
            ON UPDATE RESTRICT;
