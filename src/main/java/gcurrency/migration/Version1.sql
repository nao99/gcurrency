CREATE TABLE IF NOT EXISTS currency (
     id   SERIAL PRIMARY KEY ,
     name VARCHAR (65) NOT NULL
);

CREATE TABLE currency_course (
     id                        SERIAL PRIMARY KEY,
     dependent_currency_id     INTEGER NOT NULL
         CONSTRAINT FK_currency_course_dependent__currency
             REFERENCES currency (id)
                 ON DELETE RESTRICT
                 ON UPDATE RESTRICT,
     independent_currency_id   INTEGER NOT NULL
         CONSTRAINT FK_currency_course_independent__currency
             REFERENCES currency (id)
                 ON DELETE RESTRICT
                 ON UPDATE RESTRICT,
     dependent_currency_cost   DOUBLE PRECISION NOT NULL,
     independent_currency_cost DOUBLE PRECISION NOT NULL,
     coefficient               DOUBLE PRECISION NOT NULL,
     created_at                TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     updated_at                TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ;

CREATE TABLE user_group (
    id   SERIAL PRIMARY KEY,
    name VARCHAR (25) NOT NULL
) ;

CREATE TABLE "user" (
    id            SERIAL PRIMARY KEY,
    user_group_id INTEGER NOT NULL
        CONSTRAINT FK_user__user_group
            REFERENCES user_group (id)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT,
    name          VARCHAR (32),
    login         VARCHAR (32),
    ip_address    VARCHAR (15),
    created_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ;

CREATE TABLE user_currency (
    id          SERIAL PRIMARY KEY,
    user_id     INTEGER NOT NULL
        CONSTRAINT FK_user_currency__user
           REFERENCES "user" (id)
               ON DELETE CASCADE
               ON UPDATE RESTRICT,
    currency_id INTEGER NOT NULL
        CONSTRAINT FK_user_currency__currency
           REFERENCES currency (id)
               ON DELETE RESTRICT
               ON UPDATE RESTRICT,
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ;
