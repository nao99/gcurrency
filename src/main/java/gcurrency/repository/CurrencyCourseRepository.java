package gcurrency.repository;

import gcurrency.entity.CurrencyCourse;
import org.springframework.data.repository.CrudRepository;

public interface CurrencyCourseRepository extends CrudRepository<CurrencyCourse, Integer> {

}
