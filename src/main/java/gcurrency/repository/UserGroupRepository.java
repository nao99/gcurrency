package gcurrency.repository;

import gcurrency.entity.UserGroup;
import org.springframework.data.repository.CrudRepository;

public interface UserGroupRepository extends CrudRepository<UserGroup, Integer> {

}
