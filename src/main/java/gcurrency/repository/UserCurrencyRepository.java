package gcurrency.repository;

import gcurrency.entity.UserCurrency;
import org.springframework.data.repository.CrudRepository;

public interface UserCurrencyRepository extends CrudRepository<UserCurrency, Integer> {

}
