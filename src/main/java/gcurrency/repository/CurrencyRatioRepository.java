package gcurrency.repository;

import gcurrency.entity.CurrencyRatio;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CurrencyRatioRepository extends CrudRepository<CurrencyRatio, Integer> {
    public List<CurrencyRatio> findAllByIsTracked(Boolean isTracked);
}
