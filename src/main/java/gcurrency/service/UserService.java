package gcurrency.service;

import gcurrency.entity.User;
import gcurrency.repository.UserGroupRepository;
import gcurrency.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;
    private UserGroupRepository userGroupRepository;

    /**
     * UserService constructor
     */
    @Autowired
    public UserService(UserRepository userRepository, UserGroupRepository userGroupRepository) {
        this.userRepository = userRepository;
        this.userGroupRepository = userGroupRepository;
    }

    public Optional<User> getUser(Integer id) {
        return this.userRepository.findById(id);
    }
}
