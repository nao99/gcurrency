package gcurrency.service;

import gcurrency.entity.CurrencyCourse;
import gcurrency.entity.CurrencyRatio;
import gcurrency.repository.CurrencyCourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyCourseService {
    private CurrencyCourseRepository currencyCourseRepository;

    /**
     * CurrencyCourseService constructor
     */
    @Autowired
    public CurrencyCourseService(CurrencyCourseRepository currencyCourseRepository) {
        this.currencyCourseRepository = currencyCourseRepository;
    }

    public CurrencyCourse createCourse(CurrencyRatio currencyRatio, Double coefficient) {
        CurrencyCourse currencyCourse = new CurrencyCourse(currencyRatio, coefficient);

        return this.currencyCourseRepository.save(currencyCourse);
    }
}
