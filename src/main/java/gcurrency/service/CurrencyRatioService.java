package gcurrency.service;

import gcurrency.entity.CurrencyRatio;
import gcurrency.repository.CurrencyRatioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyRatioService {
    private CurrencyRatioRepository currencyRatioRepository;

    /**
     * CurrencyCourseService constructor
     */
    @Autowired
    public CurrencyRatioService(CurrencyRatioRepository currencyRatioRepository) {
        this.currencyRatioRepository = currencyRatioRepository;
    }

    public List<CurrencyRatio> getTrackedCurrencyRatios() {
        return this.currencyRatioRepository.findAllByIsTracked(Boolean.TRUE);
    }
}
